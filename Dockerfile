ARG VERSION=latest

FROM nginx:${VERSION}

COPY install.sh /root/

RUN /bin/bash /root/install.sh

CMD ["/usr/local/bin/nginx-foreground"]
