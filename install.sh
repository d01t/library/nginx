#!/bin/bash
cat <<EOF > /etc/nginx/conf.d/default.conf
server {
    listen 80;
    server_name localhost;
    root /var/www/html;

    location / {
        index index.html index.htm;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }

    #location ~ \.php$ {
    #    root   /var/www/html;
    #    fastcgi_pass 127.0.0.1:9000;
    #    fastcgi_index index.php;
    #    fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    #    include fastcgi_params;
    #}

    location ~ /\.ht {
        deny  all;
    }

    location ~ \.(?:css|js)$ {
        expires max;
        log_not_found off;
        access_log off;
        add_header X-Content-Type-Options "nosniff";
    }

    location ~ \.(?:ttf|ttc|eot|woff|woff2|otf|svg)$ {
        add_header Access-Control-Allow-Origin "*";
        expires max;
        log_not_found off;
        access_log off;
    }

    location ~ \.(?:jpg|jpeg|gif|png|ico)$ {
        expires max;
        log_not_found off;
        access_log off;
    }

    location ~ \.(?:rss|atom)$ {
        expires 600s;
    }
}
EOF

cat <<EOF > /etc/nginx/conf.d/gzip.conf
gzip on;

gzip_disable "msie6";

gzip_vary on;
gzip_proxied any;
gzip_comp_level 6;
gzip_buffers 16 8k;

gzip_types
    text/plain
    text/css
    text/xml
    text/javascript
    image/svg+xml
    application/json
    application/javascript
    application/x-javascript
    application/xml
    application/atom+xml
    application/xml+rss;
EOF

cat <<EOF > /usr/local/bin/nginx-foreground
#!/bin/sh
set -e

if [ -n "\${DOCUMENT_ROOT_SUBDIR}" ]
then
	sed -i -E "s#root /var/www/html#root /var/www/html/\${DOCUMENT_ROOT_SUBDIR}#g" /etc/nginx/conf.d/default.conf
fi

if [ -n "\${SERVICE_PHP_FPM}" ]
then
	sed -i -e 's#index index.html index.htm;#index index.html index.htm index.php;'"\n"'        try_files $uri $uri/ /index.php?$args ;#g' -e 's/#//g' -e "s#fastcgi_pass 127.0.0.1:9000#fastcgi_pass \${SERVICE_PHP_FPM}:9000#g" /etc/nginx/conf.d/default.conf
fi

exec nginx -g "daemon off;"
EOF

chmod +x /usr/local/bin/nginx-foreground
